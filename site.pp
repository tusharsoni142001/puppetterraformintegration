node default{

package {'nginx':
ensure =>installed,
}

file{'/tmp/status.txt':
content =>'Nginx Installed using terraform',
mode =>'0644',

}
service { 'nginx':
        ensure => running,
        enable => true,
    }
}
